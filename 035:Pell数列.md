# 035:Pell数列
## 总时间限制: 3000ms 内存限制: 65536kB
## 描述
![输入图片说明](https://git.oschina.net/uploads/images/2017/0914/110209_c6dce4a1_1520126.png "Q035.png")
### 输入
第1行是测试数据的组数n，后面跟着n行输入。每组测试数据占1行，包括一个正整数k (1 ≤ k < 1000000)。
### 输出
n行，每行输出对应一个输入。输出应是一个非负整数。
### 样例输入
```
2
1
8
```
### 样例输出
```
1
408
```
### 全局题号
1788
## 题解
```
#include <cstdio>
#include <iostream>

using namespace std;

int pell(int n)
{
    int a=1, b=2, c;
    if (n==1)
        return a;
    if (n==2)
        return b;
    while (n>2)
    {
        c= (2*b+a)%32767;
        a=b;
        b=c;
        n--;
    }
    return b;
}

int main()
{
    int n, m;
    cin >> n;
    for (int i=0;i<n;i++)
    {
        cin >> m;
        cout << pell(m) <<endl;
    }
    return 0;
}
```